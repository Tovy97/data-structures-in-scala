lazy val data_structures_in_scala = (project in file("."))
  .settings(
    name := "Data Structures in Scala",
    libraryDependencies += "org.scalacheck" %% "scalacheck" % "1.14.1" % "test",
    assemblyJarName in assembly := "data_structures_in_scala.jar"
  )


